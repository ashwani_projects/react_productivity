var Tasks = [{
    id: 1,
    task: "First Task",
    assignTo:"Ashwani"
}, {
    id: 2,
    task: "2 Task",
    assignTo:"Ashwani"
}, {
    id: 3,
    task: "3rd task",
    assignTo:"Ashwani"
}, {
    id: 4,
    task: "4 Task",
    assignTo:"Ashwani"
}, {
    id: 5,
    task: "5 Task",
    assignTo:"Ashwani"
}, {
    id: 6,
    task: "6 Task",
    assignTo:"Ashwani"
}, {
    id: 7,
    task: "7 Task",
    assignTo:"Ashwani"
}, {
    id: 8,
    task: "8 Task",
    assignTo:"Ashwani"
}];

var Projects = [{
    id: 1,
    project: "First Project",
}, {
    id: 2,
    project: "2 Project",
}];

module.exports = {
    Tasks,
    Projects
};
