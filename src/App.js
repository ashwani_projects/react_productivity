import React, {Component} from 'react';
import logo from './logo.svg';
import {Text, View} from 'react-native';
import {TaskList,AddTask} from './Components/Task'
import {ProjectList,AddProject} from './Components/Project'
import {Tasks,Projects} from './AppData';
import PropTypes from 'prop-types';
import {BrowserRouter as Router, Switch, Route, Link,Redirect} from 'react-router-dom';
import './App.css';

class App extends Component {
    getChildContext(){
        return {
            tasks: Tasks,
            projects:Projects
        };
    }
    render() {
        return (
            <Router>
                <View>
                    <View style={{marginVertical:10,flexDirection:"row",justifyContent:"space-around"}}>
                        <Link to="/">Tasks</Link>
                        <Link to="/projects">Project</Link>
                    </View>
                    <View style={{flex:1}}><Route exact path="/" render={() => (
                            <Redirect to="/tasks"/>
                    )} />
                    <Route path='/tasks' component={TaskList} />
                    <Route path='/projects' component={ProjectList} />
                    <Route path='/addTask' component={AddTask} />
                    <Route path='/addProject' component={AddProject} />
                    </View>
                </View>
            </Router>
        );
    }
}

App.childContextTypes= {
    tasks: PropTypes.array,
    projects:PropTypes.array
};


export default App;


