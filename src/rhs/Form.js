import React, {Component} from 'react';
import {Text, View, ListView, TextInput, TouchableOpacity} from 'react-native';
class Form extends React.Component {
    constructor(props) {
        super(props);
        this.saveData = this.saveData.bind(this);
        this.state = {};
    }
    saveData(value) {
        let valueToInsert = {};
        if(Array.isArray(this.props.children.props.children) && this.props.children.props.children.length>0) {
            this.props.children.props.children.map((node,index)=>{
                valueToInsert = {...valueToInsert, [node.props.field]: node.props.value};
            })
        }else {
            valueToInsert = {...valueToInsert,[this.props.children.props.children.props.field]:this.props.children.props.children.props.value}
        }
        this.props.table.push(valueToInsert);
    }
    render() {
        let {children,history} = this.props;
        return (
            <View>
                {children}
                <TouchableOpacity style={{marginVertical:10}} onPress={this.saveData}><Text>SUBMIT</Text></TouchableOpacity>
                <TouchableOpacity onPress={history.goBack}>BACK</TouchableOpacity>
            </View>
        );
    }
}
export default Form;