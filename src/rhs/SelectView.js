import React, {Component} from 'react';
import {Text, View, ListView, TextInput, TouchableOpacity} from 'react-native';
import List from './List';
class SelectView extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        let {placeholder,autoFocus,field} = this.props;
        return (
            <View>
                <TextInput
                    style={{borderColor:"black",borderWidth:1}}
                    placeholder={placeholder}
                    value={this.state.value}
                    onFocus={()=>{this.setState({show:true})}}
                />
                {this.state.show && <List
                    data={["1 Hrs","2 Hrs","3 Hrs","4 Hrs" ,"5+ hrs"]}
                    renderRow={(row) => (
                        <TouchableOpacity onPress={()=>{
                            this.setState({value:row})
                            this.setState({show:false})
                            setTimeout(_=> {
                                this.props.userInput(this.state.value);
                            }, 0);
                        }}>
                            <Text>{row}</Text>
                        </TouchableOpacity>
                    )}
                />}
            </View>
        );
    }

}
export default SelectView;