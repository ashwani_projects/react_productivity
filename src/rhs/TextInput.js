import React, {Component} from 'react';
import {Text, View, ListView, TextInput, TouchableOpacity} from 'react-native';
class TextInputField extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(value, field) {
        setTimeout(_=> {
            this.props.userInput(this.state[field]);
        }, 0);
        this.setState({[field]: value});
    }
    render() {
        let {placeholder,autoFocus,field} = this.props;
        return (
            <View>
                <TextInput placeholder={placeholder} autoFocus={autoFocus}
                           onChangeText={(value) => this.handleChange(value, field)}/>
            </View>
        );
    }

}
export default TextInputField;