import React, {Component} from 'react';
import {ListView} from 'react-native';
class List extends Component{
    constructor(props,context) {
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.props.data),
        };
    }
    render(){
        let {renderRow} = this.props
        return <ListView
            dataSource={this.state.dataSource}
            renderRow={renderRow}
        />
    }
}
export default List;
