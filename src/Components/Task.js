import React from 'react';
import PropTypes from 'prop-types';
import {Text, View, ListView, TextInput, TouchableOpacity} from 'react-native';
import List from '../rhs/List';
import Form from '../rhs/Form';
import SelectView from '../rhs/SelectView';
import TextInputField from '../rhs/TextInput';
import {BrowserRouter as Router, Switch, Route, Link, Redirect} from 'react-router-dom';
class TaskList extends React.Component {
    render() {
        return (
            <View><List
                data={this.context.tasks}
                renderRow={(row) => {
                    return((
                    <View>
                        <Text>{`${row.task} : ${row.assignTo} : ${row.time || ""}`}</Text>
                    </View>
                ))}}
            />
                <View style={{marginTop:20}}><Link to='/addTask'>Add Another Task</Link></View>
            </View>
        )
    }
}
TaskList.contextTypes = {
    tasks: PropTypes.array
};

class AddTask extends React.Component {
    constructor(props) {
        super(props);
        this.extractData = this.extractData.bind(this);
        this.state = {};
    }
    extractData(value,key) {
        this.setState({[key]: value});
    }
    render() {
        return (
            <Form {...this.props} table={this.context.tasks}>
                <View>
                    <TextInputField value={this.state["task"]} userInput={(data)=>this.extractData(data,"task")} placeholder="Enter Task Name" autoFocus field={"task"} />
                    <TextInputField value={this.state["assignTo"]} userInput={(data)=>this.extractData(data,"assignTo")} placeholder="AssignTo" autoFocus field={"assignTo"} />
                    <SelectView value={this.state["time"]} userInput={(data)=>this.extractData(data,"time")} placeholder="Enter Duration" autoFocus field={"time"} />
                </View>
            </Form>
        )
    }
}
AddTask.contextTypes = {
    tasks: PropTypes.array
};
module.exports = {
    TaskList,
    AddTask
};
