import React from 'react';
import PropTypes from 'prop-types';
import {Text, View, ListView, TextInput, TouchableOpacity} from 'react-native';
import List from '../rhs/List';
import Form from '../rhs/Form';
import TextInputField from '../rhs/TextInput';
import {BrowserRouter as Router, Switch, Route, Link, Redirect} from 'react-router-dom';
class ProjectList extends React.Component {
    render() {
        return (
            <View><List
                data={this.context.projects}
                renderRow={(row) => (
                    <View>
                        <Text>{`${row.project}`}</Text>
                    </View>
                )}
            />
                <View style={{marginTop:20}}><Link to='/addProject'>Add Another Project</Link></View>
            </View>
        )
    }
}
ProjectList.contextTypes = {
    projects: PropTypes.array
};

class AddProject extends React.Component {
    constructor(props) {
        super(props);
        this.extractData = this.extractData.bind(this);
        this.state = {};
    }
    extractData(value,key) {
        this.setState({[key]: value});
    }
    render() {
        return (
            <Form {...this.props} table={this.context.projects}>
                <View>
                    <TextInputField value={this.state["project"]} userInput={(data)=>this.extractData(data,"project")} placeholder="Enter Project Name" autoFocus field={"project"} />
                </View>
            </Form>
        )
    }
}
AddProject.contextTypes = {
    projects: PropTypes.array
};
module.exports = {
    ProjectList,
    AddProject
};
